# prueba_repo_fpga

Este es un repo de prueba

Esta es una frase que me gustaría contribuir:

Nunca perseguí la gloria
ni dejar en la memoria
de los hombres mi canción

Yo amo los mundos sutiles,
ingrávidos y gentiles
como pompas de jabón

Ahí va tu poema de Quevedo:

Vencida por la edad sentí mi espada
Y no hallé cosa en que posar mis ojos
que no fuera
recuerdo de la muerte

Amar la trama más que el desenlace